# OpenML dataset: Meta_Album_TEX_Extended

https://www.openml.org/d/44322

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Textures Dataset (Extended)**
***
The original Textures dataset is a combination of 4 texture datasets: KTH-TIPS and KTH-TIPS 2 (https://www.csc.kth.se/cvap/databases/kth-tips/index.html), Kylberg Textures Dataset (http://www.cb.uu.se/~gustaf/texture/) and UIUC Textures Dataset. The data in all four datasets is collected in laboratory conditions, i.e., images were captured in a controlled environment with configurable brightness, luminosity, scale and angle. The KTH-TIPS dataset was collected by Mario Fritz and KTH-TIPS 2 dataset was collected by P. Mallikarjuna and Alireza Tavakoli Targhi, created in 2004 and 2006 respectively. Both of these datasets were prepared under the supervision of Eric Hayman and Barbara Caputo. The data for Kylberg Textures Dataset and UIUC Textures Dataset data was collected by the original authors of these datasets in September 2010 and August 2005 respectively. The Meta-Album Textures dataset is a preprocessed version of the original dataset (combination of 4 datasets). All the images are preprocessed by first cropping into perfect squared images and then resized into 128x128 with an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/TEX.png)

**Meta Album ID**: MNF.TEX  
**Meta Album URL**: [https://meta-album.github.io/datasets/TEX.html](https://meta-album.github.io/datasets/TEX.html)  
**Domain ID**: MNF  
**Domain Name**: Manufacturing  
**Dataset ID**: TEX  
**Dataset Name**: Textures  
**Short Description**: Textures dataset from KTH-TIPS, Kylberg and UIUC  
**\# Classes**: 64  
**\# Images**: 8675  
**Keywords**: textures, manufacturing  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC BY NC 4.0, public for research, cite paper to use dataset  
**License URL(original data release)**: https://creativecommons.org/licenses/by-nc/4.0/
https://kylberg.org/wp-content/uploads/2020/05/KylbergTextureDocumentation-1.0.pdf
 
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: KTH-TIPS, Kylberg, UIUC  
**Source URL**: https://www.csc.kth.se/cvap/databases/kth-tips/index.html  
https://www.cb.uu.se/~gustaf/texture/  
  
**Original Author**: Eric Hayman, Barbara Caputo, Mario Fritz, P. Mallikarjuna, Alireza Tavakoli Targhi, Jean Ponce, Gustaf Kylberg, Svetlana Lazebnik, Cordelia Schmid  
**Original contact**: KTH-TIPS: hayman@nada.kth.se, Kylberg: gustaf.kylberg@gmail.com, UIUC: Jean.Ponce@inria.fr  

**Meta Album author**: Ihsan Ullah  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{Fritz2004THEKD,
  title={THE KTH-TIPS database},
  author={Mario Fritz and E. Hayman and B. Caputo and J. Eklundh},
  year={2004},
  url = {https://www.csc.kth.se/cvap/databases/kth-tips/index.html}
}

@inproceedings{Mallikarjuna2006THEK2,
  title={THE KTH-TIPS 2 database},
  author={P. Mallikarjuna and Alireza Tavakoli Targhi and Mario Fritz and E. Hayman and B. Caputo and J. Eklundh},
  year={2006},
  url = {https://www.csc.kth.se/cvap/databases/kth-tips/index.html}
}

@TECHREPORT{Kylberg2011c,
  author = {Gustaf Kylberg},
  title = {The Kylberg Texture Dataset v. 1.0},
  institution = {Centre for Image Analysis, Swedish University of Agricultural Sciences
	and Uppsala University, Uppsala, Sweden},
  year = {2011},
  type = {External report (Blue series)},
  number = {35},
  month = {September},
  url = {http://www.cb.uu.se/~gustaf/texture/}
}

@article{lazebnik:inria-00548530,
  TITLE = {{A sparse texture representation using local affine regions}},
  AUTHOR = {Lazebnik, Svetlana and Schmid, Cordelia and Ponce, Jean},
  URL = {https://hal.inria.fr/inria-00548530},
  JOURNAL = {{IEEE Transactions on Pattern Analysis and Machine Intelligence}},
  PUBLISHER = {{Institute of Electrical and Electronics Engineers}},
  VOLUME = {27},
  NUMBER = {8},
  PAGES = {1265--1278},
  YEAR = {2005},
  MONTH = Aug,
  DOI = {10.1109/TPAMI.2005.151},
  KEYWORDS = {Index Terms- Image processing and computer vision ; feature measurement ; pattern recognition ; texture},
  PDF = {https://hal.inria.fr/inria-00548530/file/lana_pami_final.pdf},
  HAL_ID = {inria-00548530},
  HAL_VERSION = {v1},
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44244)  [[Mini]](https://www.openml.org/d/44288)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44322) of an [OpenML dataset](https://www.openml.org/d/44322). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44322/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44322/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44322/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

